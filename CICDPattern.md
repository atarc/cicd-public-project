# Patterns: DevSecOps.CICD

`This Document has been converted from the original MS Word document stored in the ATARC Huddle site.  This is a Read Only version made available for visitors of this repository to see and consume the work performed there.`

The DevSecOps.CICD Pattern will consist of sub-patterns (aka 'stages')
such as:

Source-Pull, Build, Code-Coverage,
Quality-Scan, Security-Scan, Push-to-artifact-Rep,
Pull-from-artifact-Repo, Deploy, Smoke-test,
Monitoring.

This DevSecOps.CICD Pattern should be seen as a WIP, the above stages
may be added, expanded, or removed based on input from the working
group.

Note to Authors: Please add your name to the section if you make changes so that we can
quickly understand who to collaborate with if necessary. This is a
collaborative document, do not hesitate to make changes, ask questions
or provide suggestions.

[[_TOC_]]

## Stage: DevSecOps.CICD.TEMPLATE

**Pattern Name and Classification:** A descriptive and unique name
    that helps in identifying and referring to the pattern.

**Intent:** A description of the goal behind the pattern and the
    reason for using it.

**Also Known As:** Other names for the pattern.

**Motivation (Forces):** A scenario consisting of a problem and a
    context in which this pattern can be used.

**Applicability:** Situations in which this pattern is usable; the
    context for the pattern.

**Structure:** A graphical representation of the pattern. [Class
    diagrams](https://en.wikipedia.org/wiki/Unified_Modeling_Language#UML_Class_Diagram) and [Interaction
    diagrams](https://en.wikipedia.org/wiki/Interaction_diagram) may
    be used for this purpose.

**Participants:** A listing of the classes and objects used in the
     pattern and their roles in the design.

**Collaboration:** A description of how participants interact with
     each other.

**Consequences:** A description of the results, side effects, and
     trade-offs caused by using the pattern.

**Implementation:** A description of an implementation of the
     pattern; the solution part of the pattern. If there is any
     available, **Sample Code or Psuedocode** may be presented here.

**Known Uses:** Examples of real usages of the pattern.

**Related Sub-Patterns** Other patterns that have some relationship
     with the pattern; discussion of the differences between the
     pattern and similar patterns.

**Metrics:** Based on expert-knowledge or other experience, how many
     minutes are saved per run? What can we do with the metrics
     collected? At what point in time are metrics collected?

## Stage: DevSecOps.CICD.Source-Pull
**{Completed by Bill Schwartz}**

**Pattern Name and Classification:** Pull Source Code from Source
     Code Repository.

**Intent:** To ensure that all source code and configurations
    (excluding credentials) required to build and test a build
    artifact are pulled from a source code repository (e.g. Git,
    GitHub, BitBucket, etc.) where it can be version-controlled and
    all changes tracked.

**Also Known As:** Pull Source

**Motivation (Forces):** 
* You need a reliable means of pulling in source code into your work area.
* You need the ability to uniquely identify different versions of
        code and have the ability to pull in a specific version of code
        into your area at any given time.
* You need to have confidence these artifacts will build the
        expected application.

**Applicability:** 
* Building applications (e.g. Java application, C# application,
        TypeScript application)
* Building infrastructure (e.g. Infrastructure as Code commands in
        Terraform or Ansible that build out virtual machines, load
        balancers, etc.)

**Structure:** 

The illustration below shows how pull source is used in a workflow. In
this case, it's the first stage of a Continuous Integration (CI)
pipeline that delivers built artifacts to a repository:

![](./images/media/image1.png)

**Participants:** A listing of the classes and objects used in the
     pattern and their roles in the design.

* **Source Code Repository:** This is a single persistent location
        where all of the code artifacts reside. It is responsible not
        only for keeping these artifacts safe and serving them up to the
        client when needed, it is also responsible for tracking changes
        and reporting them when requested.

* **Client:** This is the entity that make requests for artifacts
        from the source code repository or stores artifacts to them. The
        client also makes requests for audit history as to what changes
        have been made to an artifact. In the example above, the client
        would be the CI pipeline.

* **Worker:** This is the entity that acts upon the delivered
        artifacts to produce a meaningful result. In the CI pipeline
        above, the worker would be the next stage (Build -- Compile and
        unit test).

**Collaboration:** See explanation in the participants section for
     details.

**Consequences:** 

* **Secure, reliable means of obtaining build artifacts:** Having
        all code stored in a single location ensures that everything
        needed to build an artifact is present. Otherwise, the needed
        code could come from other sources, which can be cumbersome and
        error-prone.

* **Code are under version control**: This gives the client the
        ability to obtain the artifacts needed to build an artifact as
        it was at a certain time in the past. In addition, the SCM
        produces what changes have been made to artifacts between each
        release.

* **Acceptable Reporting Mechanism for Audits:** Virtually all
        source code repository vendors maintain functionality needed to
        capture and report on what changes were made to an artifact,
        when it was changed and by whom.

* **Not Easily Accessible:** Getting access to the source code
        repository requires a certain amount of on-boarding for the
        client or worker (e.g. credentials, roles, etc.).

**Implementation:** A description of an implementation of the
     pattern; the solution part of the pattern.

**Sample Code:** 

 The following Groovy sample code is pulling artifacts from a git
 repository:
```
def call(credentialId, repoURL, branchName)
{
checkout([$class: 'GitSCM', branches: [[name: branchName]], 
doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], 
  userRemoteConfigs: [[credentialsId: credentialId , url: repoURL]]])
}
```
**Known Uses**

* Building applications (e.g. Java application, C# application,
        TypeScript application)

* Building infrastructure (e.g. Infrastructure as Code commands in
        Terraform or Ansible that build out virtual machines, load
        balancers, etc.)

**Related Sub-Patterns** 

* **GitFlow Version Control**
* **Trunk-based Version Control**

**Metrics:**

* **Change History:** How many changes have been made to a
        repository for a given time period?
* **SLOC:** How many lines of code make up an application whose
        entire configuration resides in the source code repository?

**DONE:**

a.  A process (ex: Jenkins job) pulls the source code needed to compile,
    package and deploy project from the software repository to the CI
    server. 

b.  Source code exists on CI Server. 

c.  Output: A link to recent changes is available as a persistent
    artifact (ex: Archive the Artifacts (**ATA**)) named
    changeset-final.zip.


## Stage: DevSecOps.CICD.Build
**{Completed}**

**Intent:** Help in standardizing the Build (compile) stage of the
DevSecOps pattern. This includes standard inputs, outputs. Experts could
use this pattern for mock-ups, others could use it for a set standard
and doorway into an enterprise standard DevSecOps pipeline. This is
where the application and underlying infrastructure (and potentially any
vulnerabilities) are constructed.

**Also Known As:** Build, Builder, Artifact-Packaging, compilation

**Motivation (Forces):** Codify the many different ways to build and
package a software artifact.

**Applicability:** When software/tests need to compiled and/or packaged.

**Structure:** A graphical representation of the pattern. Class diagrams
and Interaction diagrams may be used for this purpose.

![](./images/media/image2.png)

**Participants:** A listing of the classes and objects used in the
pattern and their roles in the design.

* **Client:** The CI server that performs/orchastrates the software
    compile.

* **Worker:** This is the entity that acts upon the delivered
    artifacts to produce a meaningful result. In the CI pipeline above,
    the worker would be the next stage (Build -- Compile and unit test).
    This has the necessary build tools and configuration (e.g. one for
    Java apps, one for C# etc.) and able to pull dependencies from a
    specified repository (e.g. Nexus, Artifactory, etc.)

**Collaboration:**

1.  The CI server (e.g. Jenkins, Bamboo) connects to a source code
    repository (e.g. GitHub) to extract the codebase necessary to
    complete the compile.

2.  The agent connects to the package repository (e.g. Nexus) to push
    the new artifacts. The agent also requests dependencies from the
    same or similar repositories.

**Consequences:**

1.  Results from this pattern include a compiled artifact which includes
    the unit tests that can be run.
2.  This may produce a software artifact that does or does not meet the
    acceptance criteria.

**Implementation:**

1.  Checkout code from source code repository and place into the
    workspace
2.  Initialize parameters
3.  Pull dependencies into workspace
4.  Compile and Package
5.  Unit test (if applicable)
6.  Push to artifact repo

**Sample Code:**

Examples:

a.  `javac myproject.java`
b.  `Ant <buildall>`

**Known Uses:**

1.  Any software that requires a compile or packaging includes this
    step. In this case, most continuous integration pipeline includes
    this stage.

2.  The build stage of the DevSecOps pipeline at ##### government
    agency(ies), as well as published Government repositories (e.g.
    <https://github.com/18F/cg-provision>)

**Related Sub-Patterns:**

The overarching DevSecOps pipeline pattern (yes it does not exist yet),
the"Builder" pattern (<https://en.wikipedia.org/wiki/Builder_pattern>)
-- the all DevSecOps pattern may be inherited from the Gang of Four
'Builder Pattern'.

**Metrics:**

1.  Build time
2.  Count of errors and warning
3.  Unit test results (if applicable)
4.  Number of features and/or bug fixes in build
5.  Build frequency

**DONE** (build):

a. Compiled and deployable project artifacts are created (e.g.,
    EAR/JAR/exe/dll).

b. A master compile script must exist off of the root of the project's
    master directory/trunk and it must be named build.xml or pom.xml

c. Project has secure access, via something like Archive the Artifacts
    (ATA) or an artifact repository (ex: nexus) to deployable artifacts
    needed to deliver working software, this includes, but is not
    limited to, items that are generated as a result of compilation
    and/or packaging; 

d.  All project compilation and packaging are triggered/called via
    something similar to a Jenkins job (not CRON).

e.  A log of each job (console log) is archived via ATA.

f.  Failure notifications go out to designated point persons on the dev team

g.  The Master Log (ML) contains the email addresses of all notified users

h.  Compile runs at least daily and preferably at each check-in. 

i.  If another process triggers this build, comments must be entered
    into the 'build triggers' area to indicate this (ex: '##This build
    is run via the #DEF# tool on abc.xyz.gov')

**Next Steps:**

* Execute at least one unit test with no human intervention with each
    build/compile.
* A log exists and is saved with Master Log (ML) in ATA.
* Output appears in standard view of OV.
* Publish CS/Failed metric to a metrics repo. 
* Pull a COTS/OTS/GOTS package (ex: log4j) from a repository.
* Add run-time label for version verification that maps to build time,
    code version, build job number.

## Stage: DevSecOps.CICD.Code-Coverage 
**{Completed by Yolanda Darricarrere, Majed Ayoub}**

**Pattern Name and Classification:** Code coverage, ~~Test
    coverage~~, Code coverage test

![](./images/media/image3.png)

**Intent:**  As the name suggests, "code coverage" falls under the
    umbrella of code-based testing techniques, and like most
    engineering concepts, code coverage includes mathematical
    variables (metrics) for qualitative (test coverage) and
    quantitative (code coverage) outcomes percentage-wise. In its
    simplest form, code coverage tells us how much of the source code
    is covered by a set of test cases. In its totality, a code
    coverage system gathers information about the running program,
    combines it with source code information, and generates reports
    based on the outcome of the test suite and the test bench, re: the
    test environment in conjunction with the appropriate
    software/hardware product tools). The goal (intent) of this metric
    strategy aims to measure number of lines covered by the test
    cases. It reports total number of lines in the code and number of
    lines executed by tests.

* **A Note/Distinction about Test Coverage** aims to measure the
    effectiveness of testing in a qualitative manner. It determines
    whether the test cases are covering entire functional requirements.
    Also deemed as 'black box testing', where test cases are not written
    based on code but based on user requirements or expected
    functionality.

  * It can be measured by implementing the following formula:
    * *Test Coverage = Number of detected faults/number of
            predicted defects.*
  * Another important formula that is used while calculating this
        metric is:
    * *Requirement Coverage = (Number of requirements covered /
            Total number of requirements) x 100*
  * *Visual diagram (Test Driven Development (TDD)) workflow
        example:*

![](./images/media/image4.png)


![](./images/media/image5.png)

*Sample from Salesforce Apex*

* ***Notes on Code Coverage (White-box testing):*** At the time of
    writing test cases, include as many criteria (rules/requirements a
    test suite needs to satisfy) and scenarios as possible for maximum
    code coverage; i.e. test cases should be written to cover all
    statements, functions, conditions, paths, decisions, loops,
    parameter value, entry and exit criteria.
* **There are simple (common-use) examples below, and more complex
    (less-used) code coverage methods/types further below (*but
    shouldn't all code be tested?):**

  * ***Statement Coverage*** -- *includes, unused statements, dead
        code, unused branches, and missing statements, as highlighted
        above*
  * ***Branch / Decision Coverage*** - *Every decision is executed
        with both possible outcomes -- TRUE and FALSE. For Loops- bypass
        it, execute the body and return to the beginning. (Example in
        C#)*

 ![](./images/media/image6.png) 

* ***Condition / Unconditional Branch Coverage*** - *Sometimes your
    conditions depend on several smaller conditions, called atomic
    parts. An atomic is a condition that has no logical operations such
    as AND, OR, NOT, but can include "\<", "\>" or "=". One condition
    can consist of multiple atomic parts.*

![](./images/media/image7.png)

*In this scenario, you would need two tests to accomplish 100%
    Branch Condition Coverage*. *Check out
    <https://dzone.com/articles/types-code-coverage-examples-c> for
    more complex code examples requiring 3 and 4 tests for 100%
    coverage. Or see <http://www.wikiwand.com/en/Code_coverage> for
    even more complex examples, requiring up to 8 tests (re: multiple
    condition coverage):*

  * *Edge coverage - has every edge in the Control flow graph (CFG) been
    executed?*
  * *Parameter value coverage (PVC) - all common possible values for a
    parameter are tested*
  * *Linear Code Sequence and Jump (LCSAJ) coverage a.k.a. JJ-Path
    coverage -- has every LCSAJ/JJ-path been executed?*
  * *Path coverage -- Has every possible route through a given part of
    the code been executed?*
  * *Entry/exit coverage -- Has every possible call and return of the
    function been executed?*
  * *Loop coverage -- Has every possible loop been executed zero, one,
    or more than one times?*
  * *State coverage -- Has each state in a finite-state machine been
    reached/explored?*
  * *Data-flow coverage -- Has each variable definition and its usage
    been reached/explored?*
  * *Expression coverage: Have all expressions that could affect a
    branch been executed?*
  * *State / Transition coverage: Have all states of a state machine
    been active and have all transitions between states been traversed?*
  * Toggle coverage: Have all variables or all bits of all variables
    changed state and or been through all transitions?*

**Also Known As:** Code coverage analysis, Test coverage analysis

**Motivation (Forces):** Code coverage analysis is used to measure
    the quality of software testing, usually using (dynamic) execution
    flow analysis. As highlighted above, there are many different
    types of code coverage analysis, some very basic and others that
    are very rigorous and complicated to perform without the use of
    advanced tool support.

**Applicability:** Remember that code coverage is a measure of how
    much code is executed during testing, while test coverage is a
    measure of how much of the feature set is covered within tests,
    each based on the various parameters the team selects. NOTE:
    *"Branch Condition"* Code Coverage is weaker than Statement
    Coverage and Branch Coverage as this coverage type is not required
    to execute every statement.

**Structure:** "Design patterns are categorized as Creational,
    Structural, or Behavioral. Creational patterns are used to create
    and manage the mechanism of creating instances of classes.
    Structural patterns are used to realize the relationships among
    the entities. Behavioral design patterns deal with object
    collaboration and delegation of responsibilities." (InfoWorld,
    2015)

***Adapter design pattern example:***

![](./images/media/image8.png)

***A Gang of Four example:***

![](./images/media/image9.png)

*Source: <https://www.dofactory.com/net/adapter-design-pattern>*

**Participants:** The classes and objects participating in the
    pattern above are:

* Target (ChemicalCompound)
  * defines the domain-specific interface that Client uses.
* Adapter (Compound)
  * adapts the interface Adaptee to the Target interface.
* Adaptee (ChemicalDatabank)
   * defines an existing interface that needs adapting.
* Client (AdapterApp)
   * collaborates with objects conforming to the Target interface.

**Collaboration:** "Leverage the Adapter design pattern to map
    incompatible interfaces, increase code coverage, and reduce
    complexities in designs"
    (*<https://www.infoworld.com/article/2983255/working-with-the-adapter-design-pattern.html>
    ).* The Adapter pattern enables classes (*containing incompatible
    interfaces*) and their objects to communicate (Class Adapters and
    Object Adapters). An excellent use-case scenario to use the
    adapter design pattern is when you need to use a third-party
    library that is incompatible with the types you have in your
    application.

**Consequences:** Code coverage is much more 'concretely' measured
    than test coverage (black-box testing). Helps capture bugs in
    real-time via program flow.

**Implementation:** Add test scenarios to improve coverage
    percentage. Easy to setup/measure through code coverage tool
    adoption. See other recommendations and examples further below.

**Sample Code:** See earlier examples above (and below). Also see
    <https://www.dofactory.com/net/adapter-design-pattern> for
    structure code and real-world code examples in C#.

**Known Uses:** All software applications and codes must be tested.
    White-box testing (code coverage) is an important element in the
    SDLC process or more specifically, the *Test Development Lifecycle
    Process (TDLC)*, as the last phrases of the Software Testing Life
    Cycle (STLC)

> ![](./images/media/image10.png)

*Source:
<https://www.softwaretestinghelp.com/what-is-software-testing-life-cycle-stlc/>*

**Related Sub-Patterns:** There are many software testing tools on
    the market to help reach your testing goals. Thanks to solid
    standards, requirements, and best practices, there is
    little-to-no-need for customization in this regard*. \*See Test
    Explorer example in Visual Studio.*

**Metrics:** Again, code-coverage metrics are represented as the
    number of items actually tested, the items found in your code, and
    a coverage percentage (items tested / items found).

* Not all classes should be tested, but without the additional
    information required unwanted classes will be part of code
    coverage calculations.

![](./images/media/image11.png)

* EditFormModel class shown above can be left out from code coverage
    by simply adding the ExcludeFromCodeCoverage attribute.

![](./images/media/image12.png)
![](./images/media/image13.png)

* *(In MS Visual Basic)* By default, code coverage analyzes
    [all]{.underline} solution assemblies that are loaded during unit
    tests. To exclude test code from the code coverage results and only
    include application code, add the ExcludeFromCodeCoverageAttribute
    attribute to your test class:

  * ExcludeFromCodeCoverageAttribute Class
    * *Definition / Namespace: System.Diagnostics.CodeAnalysis*
    * *Assembly: System.Diagnostics.Tools.dll*
  * Specifies that the attributed code should be excluded from code
        coverage information.

![](./images/media/image14.png)

*See other examples: "How design patterns can help you in developing
unit testing-enabled applications"\
-*
<https://www.codeproject.com/articles/18913/how-design-patterns-can-help-you-in-developing-uni>

**CODE COVERAGE REPORTS: TEST EXPLORER IN VISUAL STUDIO**

*The code coverage analysis tool in Visual Studio collects data for
native and managed assemblies (.dll or .exe files).*

![](./images/media/image15.png)

**EXAMPLE OF WHY 100% TESTING CAN BE FALLIBLE:**

In Parameter value coverage (PVC), all (common) values should be tested.
I.e., common values for a string are: *1) null, 2) empty, 3) whitespace
(space, tabs, newline), 4) valid string, 5) invalid string, 6)
single-byte string, 7) double-byte string*. A parameter may also include
a series of strings. Failure to test each possible parameter value may
leave a bug. Therefore, testing only one of these could result in "100%
code coverage" as each line is covered. However, as only one of seven
options is tested, the resultant test is only 14.2% PVC.

**REMINDER NOTES IMPACTING YOUR PERCENTAGE SCORE:**

![](./images/media/image16.png)

![](./images/media/image17.png)

Partial List of Resources for **Metrics/Code Coverage/Test Types**:
Projects, Applications, and Tools

-   [[https://opensource.com/article/20/4/testing-code-coverage]{.underline}](https://opensource.com/article/20/4/testing-code-coverage)

-   [[https://opensource.com/resources/projects-and-applications]{.underline}](https://opensource.com/resources/projects-and-applications)

-   [[https://www.guru99.com/code-coverage-tools.html]{.underline}](https://www.guru99.com/code-coverage-tools.html)

-   [[https://blog.ndepend.com/guide-code-coverage-tools/]{.underline}](https://blog.ndepend.com/guide-code-coverage-tools/)

-   [[https://www.sealights.io/code-quality/code-coverage-metrics/]{.underline}](https://www.sealights.io/code-quality/code-coverage-metrics/)

-   [[https://docs.microsoft.com/en-us/visualstudio/test/using-code-coverage-to-determine-how-much-code-is-being-tested?view=vs-2019]{.underline}](https://docs.microsoft.com/en-us/visualstudio/test/using-code-coverage-to-determine-how-much-code-is-being-tested?view=vs-2019)

-   [[https://docs.microsoft.com/en-us/visualstudio/test/troubleshooting-code-coverage?view=vs-2019]{.underline}](https://docs.microsoft.com/en-us/visualstudio/test/troubleshooting-code-coverage?view=vs-2019)

-   [[https://www.techopedia.com/definition/22535/code-coverage]{.underline}](https://www.techopedia.com/definition/22535/code-coverage)

-   [[https://www.thinksys.com/qa-testing/software-testing-metrics-kpis/]{.underline}](https://www.thinksys.com/qa-testing/software-testing-metrics-kpis/)

-   [[https://www.bullseye.com/coverage.html]{.underline}](https://www.bullseye.com/coverage.html)

**DONE:**

-   The percentage of tested-lines-of-code to total-lines-of-code is
    determined and logged.
-   Respective test or scan runs at least daily (with each
    compile/build). 
-   A log exists and is saved with Master Log (ML) in ATA.
-   The On-line View (OV) includes the ability to drill down into
    results.

**Next Steps:**

-   Enforce initial threshold of 20%
-   Make Pass/Fail Threshold toggle-able.

## Stage: DevSecOps.CICD.Quality-Scan 
**{Completed by Rupesh Kumar}**

**Pattern Name and Classification:** Quality Scan

**Intent:** Shifting quality-left by providing capabilities to scan
and detect - the bugs, vulnerabilities, code duplication, technical
debt, and code smells, to produce a higher quality working software.

**Also Known As:** Code Quality Scan, Static Code Analysis

**Motivation (Forces):**

-   Adherence to 12 factor app.
-   Inconsistencies in code style conventions and standards. It can be
    as simple as enforcing consistent indentation and variable names or
    as complex as enforcing compliance with the MISRA or CERT Secure
    Coding Standards
-   Resource leaks such as a failure to release allocated memory, which
    can eventually lead to program crashes or failure to close files
-   Incorrect usage of Application Programming Interfaces (APIs)
-   Common security vulnerabilities such as those identified by the Open
    Web Application Security Project (OWASP) or Common Weakness
    Enumeration (CWE)

**Applicability:** This pattern is applicable when any source code
is written to build a working software

**Structure:** The illustration#1 below shows how code quality
scan is used in a workflow. In this case, it's the fourth stage of a
Continuous Integration (CI) pipeline.

![](./images/media/image18.png)

 The illustration#2 below shows a sample code quality scan report.

![](./images/media/image19.png)

**Participants:**

-   **CI Server:** This is the entity that make requests for performing
    code quality scan (in stage D above) on the source code pulled from
    the source code repository.
-   **Quality Scanner:** This is the entity that runs on the CI server
    and communicates with the Server to analyze projects.
-   **Quality Server:** This is the entity where the detailed code
    quality scan analysis snapshots are made available for the project
    teams to view and triage.
-   **Quality Database:** This is the entity which persists the code
    quality scan reports and snapshots.

**Collaboration:**

 ![](./images/media/image20.png)

**Consequences:** To deliver ***high-quality*** working software to
 the customer.

**Implementation:** Please see the 'Sample Code' below

**Sample Code:**
```
stage ('Stage D: Quality Scan') {
    def qualityScanner = tool 'Scanner';
    withSonarQubeEnv ('Quality Server') {
        sh \"\${scanner}/bin/sonar-scanner\"
    }
}
```
**Known Uses:**

This pattern is best utilized while software is being developed on
developers IDE and the code is integrated with the continuous
integration (CI) server.

**Related Sub-Patterns**

-   [Gang of Four Design
    Patterns](https://www.journaldev.com/31902/gangs-of-four-gof-design-patterns)
-   [Patterns in Enterprise
    Software](https://www.martinfowler.com/articles/enterprisePatterns.html)
-   [12 Factor App](https://12factor.net/)

**Metrics:**

Should be an enabler for developers to get an indicator of how
difficult it will be to test, maintain, and troubleshoot their
applications: Few code quality metrics are as follow:

-   Methods that are too large or too small.
-   Classes that are too large or too small.
-   Methods with too many parameters (more than 7 plus or minus 2).
-   Methods with too many local variables.
-   Classes with an excessively deep inheritance structure.
-   Cyclomatic complexity by counting the number of linearly independent
    paths through a program's source code.
-   Extent of code duplication in the application which increases the
    technical debt

**DONE:**

A.  The entire code based is checked against at least one of either
    in-house-rules or other industry standards (CITE TWO).

B.  A percentage of the code based that meets the measured criteria is
    logged.

C.  Respective test or scan runs at least daily. 

D.  A log exists and is saved with Master Log (ML) in ATA.

E.  The OV includes a link to the details of the quality scan.

**NEXT STEPS:**

A.  Enforce initial threshold of 20% (aka: 20% of the code has not
    quality defects)

B.  Make Pass/Fail Threshold toggle-able.

## Stage: DevSecOps.CICD.Security-Scan
** {Completed by Majed Ayoub and Paul Fox}**

**Pattern Name and Classification:** Security Scan

**Intent:** Ensure vulnerabilities and compliance policies are
    enforced within the development and continuous operations
    processes.

**Also Known As:** Unit tests, Static Application Security Test
    (White Box testing), Dynamic Application Security Testing (Black
    Box testing), Dependency Scanning, Container Scanning,
    Configuration Compliance Scanning

**Motivation (Forces):** Align with the risk-based approach of
    system development life cycles. By conducting various security
    scans locally and on the CICD server an application is less prone
    to regressions, vulnerabilities and misconfiguration. Automation
    of security scans facilitates more reliable communication and
    integration of security into DevOps life cycle. Thus facilitating
    the accelerated adoption of the service within a security
    controlled production environment.

**Applicability:** At all points of the CICD pipeline. Developer is
    running various Security Scans locally (Static Code Analysis, Unit
    tests, Configuraiton Analysis), at the time the packages are
    pulled onto the build server (vulnerability checks, malicious code
    detection, open source dependency scanning), during promotion
    (prior to entering production. Security Scans are run performed
    within development, test and production environments. Implement
    security control gates to evaluate the scan results. Provide the
    ability to pass/fail builds,continue to the next phase of the
    DevOps cycle based upon the scan findings and policy thresholds of
    acceptable risk.

**Structure:** The structure of security scanning varies. Ability to
    programmatically make pass/fail decisions based upon scan results.

**Participants:** 

* Scanning tools/server (SAST, DAST, Containers, packages, etc.)
* Developers and DevOps Engineers
  * Execute scans locally
  * Pushes new code to the pipeline
  * Review and act upon scan results
* Security Team
* Target environment deployment operations team
* Authorizing Official (Authority to Operate)

**Collaboration:** The scanning tools are accessible, integrated and
    operated within the DevOps stages. Ability to automatically
    trigger scanning based upon events, for example when the engineer
    pushes code into a source code repository. Once the code is
    compiled and the package is deployed, DAST/Functional Testing
    should occur automatically. Once the pipeline is done running the
    Development and Security Teams are responsible for reviewing and
    remediating results of the scans to get a better understanding of
    the vulnerable components throughout the pipeline and plan for
    remediation.

**Consequences:** 

* Increased risk
* Significant delivery delays
* Vulnerabilities and misconfigurations within operational environments
* Engineer hours spent writing testing code instead of developing
        features
  * As a result the Engineer will spend less time participating
            in emergency response to security breaches.
* Development is slowed down due to execution of tests upon each
        commit (locally)

**Implementation:** Implementation of Security Scanning practices
    vary based on application and organization. Common implementations
    include invocations of SAST before and during the CI stage, DAST
    at upon completing a deployment. Routine scans of the Software
    Repository upon each check-in to curb any newly introduced
    malicious/vulnerable code.

**Sample Code:**

* Freeware
* Commercial

**Known Uses:** 

* NIST Risk Management Framework
* DoD Enterprise DevSecOps Platform (DSOP)

**Related Sub-Patterns** 

**Metrics:**

* Number of security scan pass/fails
* Number of exception, Plan of Action and Milestones (POAM)

DONE:

A.  The entire code based is compared to
    [CWE](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwiP9tbB2trwAhUDGFkFHR6_D9QQFjAAegQIDRAE&url=https%3A%2F%2Fcwe.mitre.org%2F&usg=AOvVaw2PRFNX5BRQtI8OF_bHEJBL),
    [OWASP](https://owasp.org/) standards (VERIFY THESE)

B.  Respective test or scan runs at least daily. 

C.  A log exists and is saved with Master Log (ML) in ATA.

D.  The (OnLine View) OV includes a link to the details of the quality scan.

E.  Ensure the threshold for High findings is set to ZERO. 

F.  Ensure the existing number of initial medium and low severity
    vulnerabilities in the live (production) environment serve as a
    baseline that cannot be exceeded in future code deployments, so that
    the security posture of each application is either remaining the
    same or improving.

## Stage: DevSecOps.CICD.Push-to-Artifact-Repo 
**{Completed by Tyler Johnson}**

**Pattern Name and Classification:** Push to Artifact Repository

**Intent:** Best practices around artifact management and storage.
    How to integrate best practices and this process within CI/CD
    pipelines.

**Also Known As:** Artifact Management; Artifact Storage; BinOps

**Common Tools:** Sonatype Nexus; JFrog Artifactory; DockerHub

**Motivation (Forces):** In Alignment with modern disconnected
    development processes and microservice development multiple
    artifacts are necessary in releases. In order to manage and
    maintain these artifacts best practices surrounding the
    maintenance and management of these artifacts is a necessity. The
    modern SDLC needs to have a trusted store of valid release
    candidates and artifacts. This store should be auditable, secure,
    organized, and trusted.

**Applicability:** Every development pipeline should consist of code
    checkout, scan, build, and testing stages. Following the
    successful completion of these stages the object produced in the
    build stage should now be stored as an artifact. An additional
    stage 'push-to-artifact-repo' should perform this task using some
    push functionality whether that be a manual call, maven command,
    RESTful call, or CI tool specific integration. The act of pushing
    to artifact repository should only occur when all prior stages of
    checkout, build, and testing are successfully completed. The
    artifact repository should only service as an artifact management
    system for valid artifacts that can now be processed as release
    candidates across any number of testing, pre-production, and
    additional environments on the path to production. This artifact
    repository should be organized to accurately represent the status
    and availability of these artifacts based on its folder structure
    inside of the repository. Ex. Folder = QA_arts (home to all
    artifacts and specific versions of the artifact either in or
    available for the QA environment), Folder = PROD_arts (home to all
    artifacts and specific versions of the artifact either in or
    available for the Production environment), etc.

**Structure:** ![](./images/media/image21.png)

**Participants:** 

* Artifact Scanner -- Further testing and scanning that can be
        done on artifacts within the repository.
* Publisher -- Approval/CI process used to publish artifacts
        within the repo.
* Puller -- Release/deployment processes that use the artifact
        repo as a basis for software components that are then bundled
        into releases across the environments.

**Collaboration:** 

 ![](./images/media/image22.png)

**Consequences:** By integrating artifact repositories within your
    CI/CD pipelines and your overall SDLC you are introducing
    additional overhead within your processes. By not running
    pipelines all the way to production you are introducing additional
    overhead and complexity to your pipelines. This complexity will
    add more management and execution time to the overall path to
    production.

**Implementation:** Generate standard procedures to publish built
    artifacts to an artifact management repository. These procedures
    could and should be reusable parameterized functions to make the
    implementation as easy as possible for all CI/CD pipelines. By
    generating these reusable standardized procedures, you are
    providing these best practices as a service for your CI/CD
    consumers.

**Sample Code:** 

![](./images/media/image23.png)

**Known Uses:** Please see the 'Sample Code' above

**Related Patterns:** 

* Pull from Artifact Repo
* Quality Scan -- specific to artifacts
* Deploy -- related to multiple artifacts pulled and deployed across environments.

**Metrics:**

* Deployment Frequency from artifact repository.
* Number of times an artifact is pulled from repository.
* Repository specific metrics(i.e. scanning, testing, validating)

**DONE:**

A. Entire Artifact/package exist on target or staging area in the
    Repository (Ex: nexus)

B. A console log exists with the details of this step

C. The console log is saved with Master Log (ML) in ATA.

D. The log clearly shows the version and unique identity of the pulled
    artifact

E. The ML contains the email addresses of all notified users.

## Stage: DevSecOps.CICD.Pull-from-artifact-Repo
**{ Completed }**

**Pattern Name and Classification:** Pull deployment artifact from
    the artifact repository (e.g. Nexus, Docker Hub, etc.)

**Intent:** This pattern enables DevOps teams the ability to pull an
    already tested and proven deployment artifact (e.g. .war, .jar,
    .cpp, etc.) from the holding repository without having to rebuild
    from source code and test again

**Also Known As:** N/A

**Motivation (Forces):** This is typically used by IT organizations
    not as mature from an agile perspective and/or the application
    development teams still require significant manual integration and
    QA testing of a release. The same artifact is extracted and
    re-used as opposed to being rebuilt and tested each time.

**Applicability:** This pattern is used in the first stage of the
    continuous delivery cycle and prior to deployment to one or more
    environments.

**Structure:** TBD

**Participants:** 

* The delivery pipeline itself. It makes a call to the applicable
        repository (e.g. Nexus, Docker, etc.) for the desired artifact.

**Collaboration:** 

* The Continuous Integration portion of the pipeline where it is
        responsible for delivering the built and tested artifact to the
        repository:

![Diagram Description automatically
generated](./images/media/image24.jpg)

**Consequences:** 

* Enables DevOps teams to deliver the same artifact that was
        previously built and tested to multiple environments; provides
        confidence that the same artifact is consistently delivered.
* Requires DevOps teams to store and manage multiple versions of
        the application for a prolonged period of time. This introduces
        cost and storage implications.
* Can be considered a "crutch" or hindrance from AD teams to
        maturing to a level where the artifact can be built from any
        point in time and tested automatically and fully. Most mature
        agile teams can build an application artifact from any point in
        time and run a complete set of automated tests against it in a
        matter of minutes, eliminating the need to maintain a history of
        build artifacts in a repository. This is sometimes known as
        Agile teams considering the source code repository as the
        "Single source of truth".

**Implementation:**  See Applicability step in this pipeline.

**Sample Code:** See the ATARC source code repository (URL
    forthcoming)

**Known Uses:** Most CICD pipelines in use in the private or public
    sectors. Highly mature and functioning agile teams that can
    rebuild and test any application version quickly may not include
    the push and pull from an artifact repository.

**Related Patterns:**

* Push to Artifact Repo
* Quality Scan -- specific to artifacts
* Security Scan -- specific to artifacts
* Deploy -- related to multiple artifacts pulled and deployed
        across environments.

**Metrics:**

* Deployment Frequency from artifact repository.
* Number of times an artifact is pulled from repository.
* Repository specific metrics (i.e. scanning, testing, validating)

## Stage: DevSecOps.CICD.Deploy
**{Completed by Yolanda Darricarrere}**

**Pattern Name and Classification:** A descriptive and unique name
    that helps in identifying and referring to the pattern.

**Intent:** A deployment pipeline consists of two things, Chrissie
    Buchanan reminds us GitLab, 2020), re: what to do and when to do
    it:

1.  ***Jobs,*** which define *what* to do. For example, jobs that
    compile or test code.
2.  ***Stages,*** which define *when* to run the jobs. For example,
    stages that run tests after stages that compile the code.

**Also Known As:** Deployment; Deploy-to-stage; Deploy-to-prod, Run,
    Commit, etc.

**Motivation (Forces):** There are a number of motivations,
    including traffic/user migration, A/B or canary testing, real user
    acceptance, also referenced below (Applicability), Non-Functional
    Requirement (NFR) validation, feature validation, etc.

**Applicability:** Continued testing is recommended through the
    Deploy phase, most notably, when there is a change to end-user
    experience, allowing for final consumer feedback before committing
    to the full release. NOTE: If final user testing fails, a rollback
    release may be necessary.

**Structure:** A graphical representation of the pattern. [Class
    diagrams](https://en.wikipedia.org/wiki/Unified_Modeling_Language#UML_Class_Diagram) and [Interaction
    diagrams](https://en.wikipedia.org/wiki/Interaction_diagram) may
    be used for this purpose.

![](./images/media/image25.png)

**Participants:** A listing of the classes and objects used in the
    pattern and their roles in the design.

**Collaboration:** A description of how classes and objects used in
    the pattern interact with each other. There is often more than one
    deploy-environment, such as a staging or beta environment and a
    production environment.

**Consequences:** The staging environment is used internally by the
    developers and product team to ensure the product is deploying
    correctly. The production environment is the final end-user
    environment. In Microsoft Azure, you can add Deployment Gates
    (Stages):

  * ***Pre-and Post-Deployment Conditions:***

![](./images/media/image26.png)

  * *As well as, **View Release Logs:***

![](./images/media/image27.png)

**Implementation:** Every design pattern can be as unique in its
    implementation as the next. This is due to teams making different
    architectural choices, tool selections, etc. etc. 

**Sample Code:** 

![](./images/media/image28.png)

**Known Uses:** A product is ready to deploy when there is a
    runnable instance of the code that has successfully gone through
    all the tests in the testing stage.

**Related Sub-Patterns** Activities related to the Code phase, Build
    phase, Package phase, Pre-Post Deploy phase, as highlighted above,
    in the Consequences section.

**Metrics:** You should track: lead time to production; number of
    bugs; defect resolution time; regression test duration; broken
    build time; number of code branches; and production downtime
    during deployment.

![](./images/media/image29.png)
*Source:
<https://www.youtube.com/watch?v=ayKTn2ZgGJI&list=PLjNII-Jkdjfz5EXWlGMBRk63PC8uJsHMo&index=2>*

![](./images/media/image30.png)

## Stage: DevSecOps.CICD.Smoke-Test
**{Completed by Bill Schwartz}**

**Pattern Name and Classification:** Smoke Test Environment After It
    Has Been Provisioned

**Intent:** To ensure that any new instances (VMs or containers)
    that have been provisioned are active and responding to requests
    before application regression testing takes place.

**Also Known As:** Smoke Test, Health Check

**Motivation (Forces):** 

* You need to make sure a newly provisioned environment was built
        successfully and responding to requests (e.g. ping).
* You need to make sure the newly provisioned environment is
        available before starting application regression tests in order
        to not confuse infrastructure/environment issues with
        application issues.
* You need to make sure the smoke test covers all critical end
        points on the newly-provisioned endpoints are available on the
        expected ports (e.g. database, message queue, application).

**Applicability:** 

* Any time an environment's infrastructure is modified (e.g. new
        VMs, containers, security updates, etc.)
* Any time a new application is deployed onto a new or existing VM
        or container instance
* The smoke test execution is triggered at the load balancer level
        and the results determine whether a newly provisioned instance
        (VM, container) is put into active service (a.k.a. responding to
        application requests).
* The smoke test step should always be included in any delivery
        pipeline that modifies the operating environment in any way.

**Structure:** 

* The workflow illustrated below identifies when a smoke test is
        necessary. In this case, three separate builds of VMs are
        created, and the smoke test scripts are executed against them.
        One instance fails the smoke test, which would stop the delivery
        pipeline and notify the appropriate team(s) to investigate. The
        other two build instances pass the smoke test and when proceed
        to the next round of application-based regression testing.

![A picture containing clock, meter, computer Description
automatically generated](./images/media/image31.png)

**Participants:** 

* **Delivery Pipeline:** Any delivery pipeline will include a
        stage where the smoke test is executed immediately after a new
        VM or container instance is provisioned.
* **Application Load Balancer:** Many on-premises and cloud
        providers include an endpoint on the application load balancer
        definitions to trigger a periodic health check/smoke test as
        soon as the VM/container instance is put into service. It is
        this smoke test that determines if the instance remains healthy
        enough to stay in service or if it should be terminated and to
        attempt to create a new instance in its place.
* **REST API Endpoint:** The vehicle that triggers the sequence of
        smoke tests (e.g. test web connectivity, test database, test
        message queue availability, etc.) is typically in the form of an
        API endpoint that is called by higher level processes like the
        pipeline or load balancer.

**Collaboration:** See explanation in the participants section for details.

**Consequences:** 

* Smoke test scripts provide a reliable means of testing
        environment prior to use.
* Smoke test scripts are separate from regression and other types
        of tests, providing easier troubleshooting.
* Rely on the creator to cover all critical services needed: The
        smoke test script is only as good as it was designed. Therefore,
        all critical areas required by the application or microservice
        need to be checked in order to declare the VM or container ready
        for service.

**Implementation:** 

* Smoke tests should automatically be triggered, not manually run
        by an administrator
* Smoke test scripts should be quick to execute and complete in
        less than one second. For example, pinging the web, message
        queue and database server are acceptable smoke tests. Additional
        steps can be included as well, provided the entire smoke test
        runs in one second or less.

**Sample Code:** The following health check is written in
    NodeJs/TypeScript. It tests the database for connectivity, as well
    as the availability of a storage folder:

```
import fs from 'fs'; 
import { promisify } from 'util';

import { db } from '../db';
import { logger } from '../config/logging';

import { appSettingsService } from './appSettingsService';

class HealthService {

    async checkHealth() {


    const results = await Promise.all([
        this.checkDbHealth(),
        this.checkTmpHealth()
    ]);|

    const result = {
        dbSuccessful: results[0],
        tmpSuccessful: results[1],
        allSuccessful: results.every(row => row),
    }; 
    if (!result.allSuccessful) { 
        result['message\'] = 'Health check has failed';
    }  

    return result as typeof result & { message?: string };
    } 
    async checkDbHealth() {
        try { 
            await db.sequelize.query('select count(*) from test_table');
        } catch (err) { 
            logger.error('Error occurred while performing dbhealthcheck', { err });

            return false;
        }
    return true;
    }
    
    async checkTmpHealth(shouldDelete = true) { 
        const size = await // 1MB file
        const path = '/tmp/healthcheck.tmp';
        const fd = await promisify(fs.open)(path, 'w');
        try {
            await promisify(fs.writeFile)(fd, Buffer.alloc(+size));
            await promisify(fs.fdatasync)(fd); // This call ensures that the file has been flushed to disk 

        } catch (err) {
            logger.error('Error occurred while performing tmp healthcheck', { err });
            return false;
        } finally {
            await promisify(fs.close)(fd);
        }
        if (shouldDelete) {
            try {
                await promisify(fs.unlink)(path);       
            } catch (err) {
                logger.error(`Error trying to remove ${path}.`, err);
                // intentionally not considering this an error
            }
        }
    return true;
    }
}
    export const healthService = new HealthService();
```
**Known Uses:** See participants section for examples.

**Related Sub-Patterns** 

**Metrics:** Most modern on-prem and cloud providers provide basic
    metrics regarding failure rates and up-time. If not, a script can
    be scanned to search for health check failures and which instances
    were impacted. At minimum, track the following for a given time
    period (e.g. day or week):
* Healthy Instance Rate
* Instance Failure Rate

## Stage: DevSecOps.CICD.Acceptance-Test
** {Drafted by Brian Sjoberg}**

**Pattern Name and Classification:** Test functional and
    non-functional aspects of the application. This includes all
    existing functional (a.k.a. requirements) and non-functional (i.e.
    load, performance, 508 compliance) tests and any new ones added
    during the latest development.

**Intent:** Constantly ensure the application still meets all the
    needs of the user. Provides fast feedback if an update fails to meet
    functional and non-functional needs. It is much quicker and cheaper
    to fix when the failure is known within minutes to hours of it
    happening.

**Also Known As:** Regression tests, behavior tests, feature tests

**Common Tools:** Selenium (Browser driver), Cucumber (Ruby, Java),
    Behave (Python)

**Motivation (Forces):** Provides a fast feedback loop if a recent
    modification (e.g. code change, library update, etc.) fails one or
    more acceptance tests. Provides confidence when refactoring or major
    restructuring of the application.

**Applicability:** 

* Any time code changes occur, the acceptance tests should be run
* Any time infrastructure (i.e. storage, memory, processing, load
        balancing) changes occur, the acceptance tests should be run
* Any time platform (e.g. OS, library updates) changes occur, the
        acceptance tests should be run

**Structure:** 

* **Participants (mainly want to ensure the right mix of skills, not
    so focused on specific roles):** 

  * User -- Ideally have a user or group of users to help define the
        examples for each feature
  * Product Owner
  * Architect
  * Tester
  * Developer
  * Business analyst

**Collaboration:** 

* User, Product Owner, Business Analyst, Tester, Developer should
        collaborate to develop the acceptance criteria for each of the
        features and talk about examples of how it will be used. These
        can then be turned into automated acceptance tests
* Product Owner, Business Analyst, Tester, Developer, Architect
        should collaborate to develop non-functional (a.k.a.
        architectural characteristics) tests where trade-offs will need
        to be made based on budget, time, and competing architectural
        characteristics (e.g. performance, scalability, security).

**Consequences:** 

* These need to be treated just like code and properly maintained
        and refactored, otherwise they will be lose their value and may
        be disabled in the pipeline
* Majority, if not all, of these tests should be automated

**Implementation:** Acceptance tests should be triggered
    automatically and be fully automated. As new functionality is
    developed acceptance tests should be written to capture business
    needs and ensure they are met at all times unless the need is
    removed in the future. At that point, the acceptance tests should
    be removed. Do not make tests dependent on each other.

  * Additionally a subset of non-destructive acceptance tests should
        be run after the production deployment. Non-destructive tests
        are tests that are still able to test a significant portion of
        the application without updating the database or altering other
        states of the application.

**Sample Code:**

* Gherkin (Business Case)
![](./images/media/image32.png)

* Glue Code
![](./images/media/image33.png)

**Known Uses:** 

**Related Sub-Patterns**:

**Metrics:**

* The amount of time saved grows exponentially when compared with
        attempting to do manual acceptance testing. Over time it is not
        possible to do all the necessary acceptance testing if done
        manually.

* Measure the amount of time to run all tests regularly and look
        to parallelize as much as possible.

* Capture the number of post-deployment defects reported to
        measure the effectiveness of new and existing regression tests.

## Stage: DevSecOps.CICD.Monitoring
 **{Completed by Yolanda Darricarrere}**

**Pattern Name and Classification:** Monitor during staging and
    production. Release or refine based on hard data.

**Intent:** To keep your CI/CD pipelines flowing smoothly and
    efficiently. Monitoring is usually integrated within the
    operational capabilities of the software application or CI/CD
    pipeline, taking place parallel to the Operate phase and
    consisting of data collection, analysis, and feedback to the start
    of the pipeline and to other phases as needed.

**Also Known As:** 

* *Automatic service registration and discovery;*
* *Automatic instrumentation and monitoring of application
        components;*
* *Automatic alerting of infrastructure and application problems*

**Motivation (Forces):** An ongoing quest for observability *("as we
    cannot say what the actual state is; we can only observe it"),*
    reliability, and to record NFRs and provide feedback, continuous
    testing, and continuous penetration testing, etc.

**Applicability:** \**Same as above and recommended as a best
    practice. \*Reminder Note: NFRs means Non-Functional Requirements,
    aka: clean code structure and overall code quality.*

**Structure:** A graphical representation of the pattern. [Class
    diagrams](https://en.wikipedia.org/wiki/Unified_Modeling_Language#UML_Class_Diagram) and [Interaction
    diagrams](https://en.wikipedia.org/wiki/Interaction_diagram) may
    be used for this purpose.

**Participants:** A listing of the classes and objects used in the
    pattern and their roles in the design.

**Collaboration:** A description of how classes and objects used in
    the pattern interact with each other.

**Consequences:** Ideally, teams can [monitor performance at any
    point in its release
    cycle](https://thinktribe.com/website-performance-monitoring/).

**Implementation:**  Perform monitoring efficiently and quickly in a
    measurable and scalable way through the following:
* *Collect the data available*
* *Collate and triage the data*
* *Remediate and automate*
* *Predict the what-if scenario*

**Sample Code:** An illustration of how the pattern can be used in a
    programming language.

**Known Uses:** Examples of real usages of the pattern.

**Related Sub-Patterns** Other patterns that have some relationship
    with the pattern; discussion of the differences between the
    pattern and similar patterns.

**Metrics:** In Microsoft Azure, the continuous monitoring template
    has four alert rules: Availability, Failed requests, Server
    response time, and Server exceptions. You can add more rules, or
    change the rule settings to meet your service level needs.

* To modify alert rule settings, select **Configure Application
    Insights Alerts**.
* The four default alert rules are created via an Inline script:

![](./images/media/image34.png)

* You can modify the script and add additional alert rules, modify the
    alert conditions, or remove alert rules that don\'t make sense for
    your deployment purposes.
