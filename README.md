# ATARC DevSecOps Public Project 

## DevSecOps Documentation Project

## Description
In this project,  DevSecOps implementation patterns are identified and documented. The goal of this project is to give asimple on-ramp to U.S. Public Sector teams that are working to implement DevSecOps. This project's initial version was focused on CI/CD implementation, but DevOps should go beyond that to include planning as the source of the ideas that will be put into practice utilizing CI/CD pipelines.

This project is a culmination of the first iteration of the work product delivered by the ATARC CICD Working group as of early 2022. 

## Introduction
Process followed:
1. The CI/CD Working group documented a [set of patterns](CICDPattern.md) as the starting point.  The patterns were a way to break down the entire process of Continuous Integration/Continuous Delivery (Deployment). 
2. The team then worked to implement in code or configuration a way to represent the stages defined in the patterns in a working project.
3. The team completed a first iteration and released a [blog](https://atarc.org/project/devops-wg-code-repository-increment-1/) and related videos demonstrating the work.  

## Roadmap
ATARC fully intends to evolve this further to the next stage and is in the process of defining the next iteration.

## Contributing
Contributions are welcome.  Please contact the ATARC Working group committee for more information on how to get engaged.


## An overview of the architecture:

```mermaid
graph LR

  SubGraph1 --> SubGraph1Flow
  subgraph "AWS"
  SubGraph1Flow(Deployment)
  SubGraph1Flow --> Smoke_Test
  Smoke_Test --> Monitor
  end

  
  subgraph "Terraform"
  SubGraph2Flow(init)
  SubGraph2Flow --> Verify
  Verify --> SubGraph1[Apply]
  end

  subgraph "Java Code"
  Node1[Build] --> Node2[Test]
  Node2 --> Node3[Create Package]
  Node3 --> SubGraph1Flow[Deploy]
  
end
```

The outcome of the initial iteration (continuing to evolve currently) are the following projects:
1. Project with the CI/CD Pipeline as described in the patterns document: https://gitlab.com/atarc/java-sample-app
2. Project with the Infrastructure-as-Code implementation that deploys the project to AWS: https://gitlab.com/atarc/iac-terraform


## Functional mapping to the Pattern stages
We have documented a mapping of all the executed parts of the implemented pipeline within the [Readme.md](https://gitlab.com/atarc/java-sample-app/-/blob/master/README.md) file in the Java Sample Project.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.


## Authors and acknowledgment
The initial work was performed under the leadership of the ATARC CICD Committee with contributions from community members.  For more information, please visit the [ATARC DevSecOps Pillar Community of Practice](https://atarc.org/event/devops-community-of-practice/)


## License
This repository and the related two code repositories (GitLab Projects [Java Sample](https://gitlab.com/atarc/java-sample-app) and [Terraform IaC](https://gitlab.com/atarc/iac-terraform) are released under GNU-GPL 3.0 and made available as is.


## Project status
The current iteration of the project has delivered a working set of functions and made them available for use for any private or public institutions interested in their first journey into DevSecOps.  The community of practice meets regularly. Visit the [community page](https://atarc.org/event/devops-community-of-practice/) for more information



